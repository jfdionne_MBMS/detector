/*
 * LOG.cpp
 *
 *  Created on: 2014-10-10
 *      Author: jfdionne
 */
#include "include/LOG.h"
#include <ctime>
#include <sys/time.h>
#include <sstream>
#include <iomanip>

CLOG& CLOG::getInstance()
{
	static CLOG instance;
	return instance;
}

CLOG::CLOG()
{
	strLevels[LOG_EMERG] = "EMERG";
	strLevels[LOG_ALERT] = "ALERT";
	strLevels[LOG_CRIT] = "CRIT";
	strLevels[LOG_ERR] = "ERR";
	strLevels[LOG_WARN] = "WARN";
	strLevels[LOG_NOTICE] = "NOTICE";
	strLevels[LOG_INFO] = "INFO";
	strLevels[LOG_DBG] = "DBG";
}

string CLOG::getTime()
{
	ostringstream ossTime;
	struct timeval nowtime;
	struct tm *timeinfo;

	gettimeofday(&nowtime, NULL);
	timeinfo = localtime(&nowtime.tv_sec);

	ossTime << (timeinfo->tm_year + 1900) << "/" << setfill('0') << setw(2) << (timeinfo->tm_mon + 1) <<
			"/" << setfill('0') << setw(2) << timeinfo->tm_mday << "_" << setfill('0') << setw(2) <<
			timeinfo->tm_hour << ":" << setfill('0') << setw(2) << timeinfo->tm_min << ":" <<
			setfill('0') << setw(2) << timeinfo->tm_sec << "." << setw(3) << (nowtime.tv_usec / 1000);

	return ossTime.str();
}
