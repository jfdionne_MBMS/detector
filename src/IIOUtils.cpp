/* IIO - useful set of util functionality
 *
 * Copyright (c) 2008 Jonathan Cameron
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

/* Made up value to limit allocation sizes */
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <cerrno>
#include <sstream>
#include <fstream>
#include "include/IIOUtils.h"
#include "include/LOG.h"

#define LOG(lvl, args...) LOGMOD("IIO", lvl, args)

const string CIIOUtils::iio_dir = "/sys/bus/iio/devices/";
const int32 CIIOUtils::IIO_EVENT_CODE_RING_50_FULL = 200;
const int32 CIIOUtils::IIO_EVENT_CODE_RING_75_FULL = 201;
const int32 CIIOUtils::IIO_EVENT_CODE_RING_100_FULL = 202;
const int32 CIIOUtils::IIO_MAX_NAME_LENGTH = 30;

CIIOUtils* CIIOUtils::m_instance = NULL;

CIIOUtils* CIIOUtils::getInstance()
{
	if(m_instance == NULL)
	{
		m_instance = new CIIOUtils;
	}
	return m_instance;
}

CIIOUtils::~CIIOUtils()
{
	delete m_instance;
}
/**
 * find_type_by_name() - function to match top level types by name
 * @name: top level type instance name
 * @type: the type of top level instance being sort
 *
 * Typical types this is used for are device and trigger.
 **/
int32 CIIOUtils::find_type_by_name(string name, string type)
{
	const struct dirent *ent;
	int32 number;
	DIR *dp;
	string thisname;
	string filename;
	ostringstream oss;
	ifstream ifs;

	dp = opendir(iio_dir.c_str());
	if (dp == NULL) {
		LOG(LOG_ERR, "No industrialio devices available" << endl);
		return -ENODEV;
	}

	while (ent = readdir(dp), ent != NULL)
	{
		string dname = ent->d_name;
		if (dname.compare(".") != 0 &&
			dname.compare("..") != 0 &&
			dname.length() > type.length() &&
			dname.compare(0, type.length(), type) == 0)
		{
			number = dname[type.length()] - '0';
			LOG(LOG_INFO, "Number is " << number << endl);
			/* verify the next character is not a colon */
			if (dname.compare(0, type.length() + 1, ":") != 0)
			{
				oss << iio_dir << type << number << "/name";
				filename = oss.str();
				ifs.open(filename.c_str());
				if (ifs.is_open())
				{
					ifs >> thisname;
					LOG(LOG_INFO, "Name is " << thisname << endl);
					if(name.compare(thisname) == 0)
					{
						ifs.close();
						closedir(dp);
						return number;
					}
				}
				ifs.close();
			}
		}
	}
	closedir(dp);
	return -ENODEV;
}

int32 CIIOUtils::write_sysfs_int(string filename, string basedir, int32 val, int32 verify)
{
	int32 ret = 0;
	FILE *sysfsfp;
	int32 test;

	string temp = basedir + "/" + filename;

	sysfsfp = fopen(temp.c_str(), "w");
	if (sysfsfp == NULL)
	{
		LOG(LOG_ERR, "Failed to open " << temp << endl);
		ret = -errno;
		return ret;
	}

	fprintf(sysfsfp, "%d", val);
	fclose(sysfsfp);
	if (verify) {
		sysfsfp = fopen(temp.c_str(), "r");
		if (sysfsfp == NULL)
		{
			LOG(LOG_ERR, "Failed to open " << temp << endl);
			ret = -errno;
			return ret;
		}
		fscanf(sysfsfp, "%d", &test);
		if (test != val)
		{
			LOG(LOG_ERR, "Possible failure in int write " << val<< " to " << basedir << "/" << filename << endl);
			ret = -1;
		}
	}
	return ret;
}

int32 CIIOUtils::write_sysfs_int(string filename, string basedir, int32 val)
{
	return write_sysfs_int(filename, basedir, val, 0);
}

int32 CIIOUtils::write_sysfs_int_and_verify(string filename, string basedir, int32 val)
{
	return write_sysfs_int(filename, basedir, val, 1);
}

int32 CIIOUtils::write_sysfs_string(string filename, string basedir, string val, int32 verify)
{
	int ret = 0;
	FILE  *sysfsfp;
	string temp = basedir + "/" + filename;

	sysfsfp = fopen(temp.c_str(), "w");
	if (sysfsfp == NULL)
	{
		LOG(LOG_ERR, "Failed to open " << temp << endl);
		ret = -errno;
		return ret;
	}

	LOG(LOG_INFO, "Val is " << val << endl);
	fprintf(sysfsfp, "%s", val.c_str());
	fclose(sysfsfp);
	if (verify) {
		sysfsfp = fopen(temp.c_str(), "r");
		if (sysfsfp == NULL) {
			ret = -errno;
			return ret;
		}
		fscanf(sysfsfp, "%s", temp.c_str());
		LOG(LOG_INFO, "Result is " << val << endl);
		if (strcmp(temp.c_str(), val.c_str()) != 0) {
			LOG(LOG_ERR, "Possible failure in string write of " << temp <<
					" Should be " << val << "writen to " << basedir << "/" << filename << endl);
			ret = -1;
		}
	}
	return ret;
}
/**
 * write_sysfs_string_and_verify() - string write, readback and verify
 * @filename: name of file to write to
 * @basedir: the sysfs directory in which the file is to be found
 * @val: the string to write
 **/
int32 CIIOUtils::write_sysfs_string_and_verify(string filename, string basedir, string val)
{
	return write_sysfs_string(filename, basedir, val, 1);
}

int32 CIIOUtils::write_sysfs_string(string filename, string basedir, string val)
{
	return write_sysfs_string(filename, basedir, val, 0);
}

int32 CIIOUtils::read_sysfs_posint(string filename, string basedir)
{
	int32 ret = 0;
	FILE  *sysfsfp;
	string temp = basedir + "/" + filename;

	sysfsfp = fopen(temp.c_str(), "r");
	if (sysfsfp == NULL)
	{
		LOG(LOG_ERR, "Failed to open " << temp << endl);
		ret = -errno;
		return ret;
	}
	fscanf(sysfsfp, "%d\n", &ret);
	fclose(sysfsfp);
	return ret;
}

int32 CIIOUtils::read_sysfs_float(string filename, string basedir, float *val)
{
	float ret = 0;
	FILE  *sysfsfp;
	string temp = basedir + "/" + filename;

	sysfsfp = fopen(temp.c_str(), "r");
	if (sysfsfp == NULL)
	{
		LOG(LOG_ERR, "Failed to open " << temp << endl);
		ret = -errno;
		return ret;
	}
	fscanf(sysfsfp, "%f\n", val);
	fclose(sysfsfp);

	return ret;
}
