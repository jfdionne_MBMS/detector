/*
 * main.cpp
 *
 *  Created on: 2014-10-10
 *      Author: jfdionne
 */
#include "include/LOG.h"
#include "include/Controller.h"
#include "include/TCPClient.h"
#include "include/ADC.h"
#include <string>
#include <unistd.h>

#define LOG(lvl, args...) LOGMOD("MAIN", lvl, args)

int main(int argc, char *argv[])
{
	auto_ptr<CController> controllerThread(new CController);
	controllerThread->start();
	controllerThread->join();

    LOG(LOG_INFO, "Program stopped!" << endl);
}
