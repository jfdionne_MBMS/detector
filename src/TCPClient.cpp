/*
 * TCPClient.cpp
 *
 *  Created on: Sep 28, 2015
 *      Author: jfdionne
 */
#include "include/TCPClient.h"
#include "include/LOG.h"
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#define LOG(lvl, args...) LOGMOD("TCP_CLIENT", lvl, args)

const int32 CTCPClient::RECEIVE_TIMEOUT = 1;

CTCPClient::CTCPClient(string ipaddress, int32 port) : m_pListener(NULL), m_clientRunning(true)
{
	memset(&m_address, 0, sizeof(m_address));
	m_address.sin_addr.s_addr = inet_addr(ipaddress.c_str());
	m_address.sin_family = AF_INET;
	m_address.sin_port = htons(port);

	m_socket = socket(AF_INET, SOCK_STREAM, 0);
	if(m_socket < 0)
	{
		LOG(LOG_ERR, "Socket failed" << endl);
		return;
	}

    // Set socket to non-blocking
    long arg = fcntl(m_socket, F_GETFL, NULL);
    arg |= O_NONBLOCK;
    fcntl(m_socket, F_SETFL, arg);
	m_state = WAIT_FOR_CONNECT;
}

CTCPClient::~CTCPClient()
{
	close(m_socket);
}

pTCPBuffer CTCPClient::getLastMessage()
{
	pTCPBuffer ret = m_pTCPBufferQueue.front();
	m_pTCPBufferQueue.pop();
	return ret;
}

void* CTCPClient::run()
{
    LOG(LOG_INFO, "TCP Client started!" << endl);
	LOG(LOG_INFO, "Connecting to server..." << endl);
	while(m_clientRunning)
	{
		switch(m_state)
		{
			case WAIT_FOR_CONNECT:
			{
				if(connect(m_socket, (struct sockaddr*)&m_address, sizeof(m_address)) < 0)
				{
					if (errno == EINPROGRESS || errno == EALREADY)
					{
						LOG(LOG_INFO, "Connection in progress. Retrying in 0.1 second..." << endl);
						usleep(100000);
					}
		            else
		            {
		            	LOG(LOG_ERR, "Connect error " << errno << " - " << strerror(errno) << endl);
		            	sleep(1);
		            }
				}
				else
				{
					LOG(LOG_INFO, "Connected to the server!" << endl);
					m_state = CONNECTED;
				}
			}
			break;
			case CONNECTED:
			{
			    fd_set sdset;
			    struct timeval tv;

			    tv.tv_sec = RECEIVE_TIMEOUT;
			    tv.tv_usec = 0;
			    FD_ZERO(&sdset);
			    FD_SET(m_socket, &sdset);
			    int ret = select(m_socket+1, &sdset, NULL, NULL, &tv);
			    if(ret < 0)
			    {
			    	LOG(LOG_ERR, "Error " << errno << " - " << strerror(errno) << endl);
			    }
			    else if(ret > 0)
			    {
					pTCPBuffer pBuffer = new TCPBuffer;
					memset(pBuffer, 0, sizeof(TCPBuffer));
					if((pBuffer->length = recv(m_socket, pBuffer->buffer, 1024, 0)) <= 0)
					{
						LOG(LOG_ERR, "Error " << errno << " - " << strerror(errno) << endl);
						delete pBuffer;
					}
					else
					{
						m_pTCPBufferQueue.push(pBuffer);
						m_pListener->TCPNotify();
					}
			    }
			}
			break;
			case STOP_CLIENT:
			{
				m_clientRunning = false;
			}
			break;
		}

	}
	return 0;
}
