/*
 * FTPClient.cpp
 *
 *  Created on: 2014-10-12
 *      Author: jfdionne
 */
#include "include/FTPClient.h"
#include "include/LOG.h"

#include <sys/stat.h>

#define LOG(lvl, args...) LOGMOD("FTP", lvl, args)

const string CFTPClient::FILE_TO_TRANFER_PATH = "/home/files/";
const string CFTPClient::REMOTE_URL = "ftp://jfdionne:slayer@192.168.0.66/";

CFTPClient::CFTPClient() : m_curl(NULL), m_res(CURLE_OK)
{
}

CFTPClient& CFTPClient::getInstance()
{
	static CFTPClient instance;
	return instance;
}

size_t CFTPClient::read_callback(void* ptr, size_t size, size_t nmemb, void* stream)
{
	return fread(ptr, size, nmemb, (FILE*)stream);
}

void CFTPClient::upload(string filename)
{
	struct stat file_info;
	curl_off_t filesize;
	FILE* fileToTransfer;
	string fullFilePath = FILE_TO_TRANFER_PATH + filename;
	string fullURL = REMOTE_URL + filename;

	if(stat(fullFilePath.c_str(), &file_info))
	{
		LOG(LOG_ERR, "Could not open " << filename << endl);
		return;
	}

	filesize = (curl_off_t)file_info.st_size;

	LOG(LOG_INFO, "File to transfer size " << filesize << "bytes" << endl);

	fileToTransfer = fopen(fullFilePath.c_str(), "rb");

	curl_global_init(CURL_GLOBAL_ALL);

	m_curl = curl_easy_init();

	if(m_curl)
	{
		curl_easy_setopt(m_curl, CURLOPT_VERBOSE, 0L);

		curl_easy_setopt(m_curl, CURLOPT_READFUNCTION, read_callback);

		curl_easy_setopt(m_curl, CURLOPT_UPLOAD, 1L);

		curl_easy_setopt(m_curl, CURLOPT_URL, fullURL.c_str());

		curl_easy_setopt(m_curl, CURLOPT_READDATA, fileToTransfer);

		curl_easy_setopt(m_curl, CURLOPT_INFILESIZE_LARGE, (curl_off_t)filesize);

		m_res = curl_easy_perform(m_curl);

		if(m_res != CURLE_OK)
		{
			LOG(LOG_ERR, "Failed to transfer file..." << endl);
		}
		else if(m_res == CURLE_OK)
		{
			LOG(LOG_INFO, "File transfered successfully!" << endl);
			remove(fullFilePath.c_str());
		}
		curl_easy_cleanup(m_curl);
	}
	fclose(fileToTransfer);

	curl_global_cleanup();
}
