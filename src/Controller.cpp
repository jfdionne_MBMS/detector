/*
 * Controller.cpp
 *
 *  Created on: Sep 29, 2015
 *      Author: jfdionne
 */

#include "include/LOG.h"
#include "include/Controller.h"
#include "include/Config.h"
#include "include/FileManager.h"
#include "include/FTPClient.h"
#include <string.h>
#include <boost/algorithm/string.hpp>

using namespace boost::algorithm;

#define LOG(lvl, args...) LOGMOD("CONTROL", lvl, args)

CController::CController()
{
	m_pTCPBuffer = NULL;
	m_bRunning = true;
	m_buffer_handler = NULL;
	channel0Vals = NULL;
	channel1Vals = NULL;
	channel2Vals = NULL;

	//Start TCP client thread
	sem_init(&m_SemReact, 0, 0);
	m_pClientThread.reset(new CTCPClient(SERVER_IPADDRESS, SERVER_PORT));
	m_pClientThread->start();
	m_pClientThread->registerListener(this);

	//Start ADC thread
	m_pADCThread.reset(new CADC());
	m_pADCThread->start();
	m_pADCThread->registerListener(this);

	//For config change notifications, only ADC is notified for now
	CConfig::getInstance().registerListener(m_pADCThread.get());
	CConfig::getInstance().registerListener(&CFileManager::getInstance());

	//File that will contain the acquisiton results
	ostringstream oss;
	oss << "/dev/iio:device" << m_pADCThread->m_dev_num;
	m_buf_name = oss.str();

	//List all remaining files and transfer them
	currentState = INIT;
	sem_post(&m_SemReact);
	m_bAcqCompletedOrStopped = false;
}

CController::~CController()
{
	sem_destroy(&m_SemReact);
}

void CController::TCPNotify()
{
	m_pTCPBuffer = m_pClientThread->getLastMessage();
	m_pTCPBuffer->buffer[m_pTCPBuffer->length] = 0;
	string strCommand(m_pTCPBuffer->buffer);
	delete m_pTCPBuffer;

	vector<string> tokens;
	split(tokens, strCommand, is_any_of(","));

	if(tokens[0] == COMMAND_CONF)
	{
		CConfig::getInstance().setParameterValue(tokens[1], tokens[2]);
		//Add the mode RT vs DEF test
	}
	else if(tokens[0] == COMMAND_ACQ_DEF)
	{
		currentState = ACQUIRE_DEF;
		if(tokens[1] == COMMAND_ACQ_START)
		{
			if(m_pADCThread->getCurrentState() == CADC::IDLE)
			{
				CFileManager::getInstance().createSEGYBaseFile();
				m_pADCThread->startRecording(false);
			}
			else if(m_pADCThread->getCurrentState() == CADC::ACQUIRE)
			{
				LOG(LOG_ERR, "Already acquiring data!" << endl);
			}
			else
			{
				LOG(LOG_ERR, "Unknown acq command!" << endl);
			}
		}
		else if(tokens[1] == COMMAND_ACQ_STOP && m_pADCThread->getCurrentState() == CADC::ACQUIRE)
		{
			m_pADCThread->stopRecording();
			//notify server??
		}
		else
		{
			LOG(LOG_ERR, "Unknown acq command!" << endl);
		}
	}
	else if(tokens[0] == COMMAND_ACQ_RT)
	{
		//Not supported yet
	}
	else
	{
		LOG(LOG_ERR, "Unknown command: " << strCommand << endl);
	}
}

void CController::ADCNotify(bool bCompletedOrStopped)
{
	m_bAcqCompletedOrStopped = bCompletedOrStopped;
	sem_post(&m_SemReact); //Start file writing process and transfer
}

void CController::stopController()
{
	m_pClientThread->stopClient();
	m_pClientThread->join();
	m_bRunning = false;
	sem_post(&m_SemReact);
}

void* CController::run()
{
	LOG(LOG_INFO, "Controller started" << endl);
	bool test = false;

	while(m_bRunning)
	{
		sem_wait(&m_SemReact);
		if(m_bRunning)
		{
			switch(currentState)
			{
				case INIT:
				{
					stringSet::iterator it;
					if(CFileManager::getInstance().init())
					{
						for (it = CFileManager::getInstance().segyFileSet.begin();
								it != CFileManager::getInstance().segyFileSet.end(); ++it)
						{
							CFTPClient::getInstance().upload(*it);
						}
					}
					currentState = ACQUIRE_DEF;
				}
				break;
				case ACQUIRE_DEF:
				{
					size_t read_size;
					uint32 sampleid = 0;

					uint32 size = m_pADCThread->m_samples_per_sec * (m_pADCThread->m_record_duration_ms / 1000);
					LOG(LOG_INFO, "Writing " << size << " samples per channel..." << endl);
					channel0Vals = new uint32[size];
					channel1Vals = new uint32[size];
					channel2Vals = new uint32[size];

					m_buffer_handler = fopen(m_buf_name.c_str(), "rb");
					if(m_buffer_handler == NULL)
					{
						LOG(LOG_ERR, "Failed to open buffer access" << endl);
					}
					else
					{
						do
						{
							read_size = fread(m_datum, 12, 1, m_buffer_handler);
							if (read_size != 1) {
								LOG(LOG_INFO, "eof reached" << endl);
								break;
							}

							if(sampleid < size)
							{
								if(m_datum[0] & 0x80)
								{
									channel0Vals[sampleid] = 0x000000ff;
								}
								else
								{
									channel0Vals[sampleid] = 0;


								}
								channel0Vals[sampleid] |= (m_datum[0] << 8) | (m_datum[1] << 16) | (m_datum[2] << 24);

								if(m_datum[3] & 0x80)
								{
									channel1Vals[sampleid] = 0x000000ff;
								}
								else
								{
									channel1Vals[sampleid] = 0;

								}
								channel1Vals[sampleid] |= (m_datum[3] << 8) | (m_datum[4] << 16) | (m_datum[5] << 24);

								if(m_datum[6] & 0x80)
								{
									channel2Vals[sampleid] = 0x000000ff;
								}
								else
								{
									channel2Vals[sampleid] = 0;
								}
								channel2Vals[sampleid] |= (m_datum[6] << 8) | (m_datum[7] << 16) | (m_datum[8] << 24);

								sampleid++;
							}
						}while(read_size);

						LOG(LOG_INFO, "End of read" << endl);
					}

					fclose(m_buffer_handler);

					//Write traces to SEGY file
					CFileManager::getInstance().appendSEGYTrace(channel0Vals, sampleid);
					CFileManager::getInstance().appendSEGYTrace(channel1Vals, sampleid);
					CFileManager::getInstance().appendSEGYTrace(channel2Vals, sampleid);
					delete channel0Vals;
					delete channel1Vals;
					delete channel2Vals;
					CFileManager::getInstance().closeSEGYFile();

					//Transfer it
					CFTPClient::getInstance().upload(CFileManager::getInstance().m_currentFilename);
				}
				break;
				case ACQUIRE_RT:
				{
					//Not supported
				}
				break;
				default:
				{
					LOG(LOG_ERR, "Unknown state" << endl);
				}
				break;
			}
		}
	}
	return 0;
}
