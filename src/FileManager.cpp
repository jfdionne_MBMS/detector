/*
 * FileManager.cpp
 *
 *  Created on: Oct 24, 2015
 *      Author: jfdionne
 */

#include "include/LOG.h"
#include "include/FileManager.h"
#include "include/SEGY.h"
#include "include/Config.h"
#include <dirent.h>
#include <fstream>
#include <string.h>
#include <ctime>
#include <sys/time.h>
#include <iomanip>
#include <byteswap.h>

#define LOG(lvl, args...) LOGMOD("FILE_MANAGER", lvl, args)

const string CFileManager::filesBaseDir = "/home/files/";
const string CFileManager::filesBaseName = "acq";
const string CFileManager::filesExtension = ".segy";

CFileManager& CFileManager::getInstance()
{
	static CFileManager instance;
	return instance;
}

CFileManager::CFileManager()
{
	m_currentFileTraceIndex = 0;
	m_samplingRate = (uint32)atoi(CConfig::getInstance().getParameterValue(CONFIG_SAMPLING_RATE).c_str());
	m_recordLength = (uint32)atoi(CConfig::getInstance().getParameterValue(CONFIG_RECORD_LENGTH_S).c_str());

	m_sampleCount = (uint16)(m_samplingRate * m_recordLength);

	float sampleInt = (float)m_samplingRate;
	m_sampleIntervalUs = (uint16)((1 / sampleInt) * 1000000);
}

bool CFileManager::init()
{
	DIR *dir;
	struct dirent *ent;
	bool ret = false;

	LOG(LOG_INFO, "Listing SEGY file directory..." << endl);
	if ((dir = opendir (filesBaseDir.c_str())) != NULL)
	{
		while ((ent = readdir (dir)) != NULL)
		{
			string str(ent->d_name);
			if(str != "." && str != "..")
			{
				LOG(LOG_INFO, filesBaseDir << str << endl);
				ret = true;
				segyFileSet.insert(str);
			}
		}
		closedir (dir);
	}
	else
	{
	  LOG(LOG_ERR, "Verify that " << filesBaseDir << " directory exists." << endl);
	}
	LOG(LOG_INFO, "Listing SEGY file directory done!" << endl);
	return ret;
}

void CFileManager::configChangedNotify()
{
	m_samplingRate = (uint32)atoi(CConfig::getInstance().getParameterValue(CONFIG_SAMPLING_RATE).c_str());
	m_recordLength = (uint32)atoi(CConfig::getInstance().getParameterValue(CONFIG_RECORD_LENGTH_S).c_str());

	m_sampleCount = (uint16)(m_samplingRate * m_recordLength);

	float sampleInt = (float)m_samplingRate;
	m_sampleIntervalUs = (uint16)((1 / sampleInt) * 1000000);
}

void CFileManager::createSEGYBaseFile()
{
	ostringstream ossTime;
	struct timeval nowtime;
	struct tm *timeinfo;
	uint16 temp16;
	uint32 temp32;

	m_currentFileTraceIndex = 0;

	gettimeofday(&nowtime, NULL);
	timeinfo = localtime(&nowtime.tv_sec);

	ossTime << filesBaseName << (timeinfo->tm_year + 1900) << setfill('0') << setw(2) << (timeinfo->tm_mon + 1) <<
			setfill('0') << setw(2) << timeinfo->tm_mday << "_" << setfill('0') << setw(2) <<
			timeinfo->tm_hour << setfill('0') << setw(2) << timeinfo->tm_min <<
			setfill('0') << setw(2) << timeinfo->tm_sec << filesExtension;

	m_currentFilename = ossTime.str();
	string fullFilename = filesBaseDir + m_currentFilename;
	ofs.open(fullFilename.c_str(), ios::out | ios::binary);

	//Textual header actually empty
	memset(TextualFileHeader, 0, TEXTUAL_FILE_HEADER_LENGTH);
	ofs.write(TextualFileHeader, TEXTUAL_FILE_HEADER_LENGTH);

	//Writing binary header
	BinaryHeader binh;
	memset(&binh, 0, sizeof(BinaryHeader));
	size_t test = sizeof(BinaryHeader);
	binh.dataTracesPerRecord = bswap_16(NUMBER_OF_CHANNELS);	//3 channels
	binh.sampleIntervalForReelUs = bswap_16((uint16)m_sampleIntervalUs);
	binh.sampleIntervalForFieldUs = binh.sampleIntervalForReelUs;
	binh.numberOfSamplesPerTraceReel = bswap_16(m_sampleCount);
	binh.numberOfSamplesPerTraceField = binh.numberOfSamplesPerTraceReel;
	binh.dataSampleFormatCode = bswap_16(2);	//4-byte 2's complement integer
	ofs.write(reinterpret_cast<const char *>(&binh), sizeof(BinaryHeader));
}

void CFileManager::appendSEGYTrace(uint32* data, uint32 length)
{
	TraceHeader traceh;
	memset(&traceh, 0, sizeof(TraceHeader));
	size_t test = sizeof(TraceHeader);
	traceh.originalFieldRecordNumber = bswap_32(1);
	traceh.traceNumberWithinFieldRecord = bswap_32((uint32)(m_currentFileTraceIndex + 1));
	traceh.traceIdentificationCode = bswap_16(1);
	traceh.verticallySummedTraces = bswap_16(1);
	traceh.groupXCoordinate = bswap_32((uint32)m_currentFileTraceIndex);
	traceh.numberOfSamplesInThisTrace = bswap_16((uint16)length);
	traceh.sampleIntervalInMsForThisTrace = bswap_16(m_sampleIntervalUs);
	traceh.geophoneGroupNumberOfRollSwitch = bswap_16((uint16)(m_currentFileTraceIndex + 2));
	traceh.geophoneGroupNumberOfTraceOne = bswap_16((uint16)(m_currentFileTraceIndex + 2));
	ofs.write(reinterpret_cast<const char *>(&traceh), sizeof(TraceHeader));
	ofs.write(reinterpret_cast<const char *>(data), length * sizeof(uint32));
	m_currentFileTraceIndex++;
}

void CFileManager::closeSEGYFile()
{
	ofs.close();
}
