/*
 * Thread.cpp
 *
 *  Created on: 2014-10-11
 *      Author: jfdionne
 */

#include "include/LOG.h"
#include "include/Thread.h"
#include <stdlib.h>
#include <cassert>

#define LOG(lvl, args...) LOGMOD("THREAD", lvl, args)

CThread::CThread(auto_ptr<CRunnable> run, bool isDetached) :
	runnable(run), detached(isDetached), result(NULL)
{
	if(!runnable.get())
	{
		LOG(LOG_ERR, "Thread::Thread(auto_ptr<CRunnable> run, bool isDetached) failed at " <<
				__FILE__ << ":" << __LINE__ << "-" <<
				" runnable is NULL" << endl);
		exit(-1);
	}
}

void* CThread::startThreadRunnable(void* pVoid)
{
	CThread* runnableThread = static_cast<CThread*>(pVoid);
	assert(runnableThread);
	runnableThread->result = runnableThread->runnable->run();
	runnableThread->setCompleted();
	return runnableThread->result;
}

void* CThread::startThread(void* pVoid)
{
	CThread* aThread = static_cast<CThread*>(pVoid);
	assert(aThread);
	aThread->result = aThread->run();
	aThread->setCompleted();
	return aThread->result;
}

//CRunnable::~CRunnable() {}
//CThread::~CThread() {}

void CThread::start()
{
	int status = pthread_attr_init(&attr);
	if(status)
	{
		LOG(LOG_ERR, "pthread_attr_init failed at " << status << __FILE__ << __LINE__);
		exit(status);
	}

	status = pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);

	if(status)
	{
		LOG(LOG_ERR, "pthread_attr_setscope failed at " << status << __FILE__ << __LINE__);
		exit(status);
	}

	if(!detached)
	{
		if(!runnable.get())
		{
			status = pthread_create(&m_thread, &attr, CThread::startThread, (void*)this);

			if(status)
			{
				LOG(LOG_ERR, "pthread_create failed at " << status << __FILE__ << __LINE__);
				exit(status);
			}
		}
		else
		{
			status = pthread_create(&m_thread, &attr, CThread::startThreadRunnable, (void*)this);

			if(status)
			{
				LOG(LOG_ERR, "pthread_create failed at " << status << __FILE__ << __LINE__);
				exit(status);
			}
		}
	}
	else
	{
		status = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

		if(status)
		{
			LOG(LOG_ERR, "pthread_attr_setdetachstate failed at " << status << __FILE__ << __LINE__);
			exit(status);
		}
		if(!runnable.get())
		{
			status = pthread_create(&m_thread, &attr, CThread::startThread, (void*)this);

			if(status)
			{
				LOG(LOG_ERR, "pthread_create failed at " << status << __FILE__ << __LINE__);
				exit(status);
			}
		}
		else
		{
			status = pthread_create(&m_thread, &attr, CThread::startThreadRunnable, (void*)this);

			if(status)
			{
				LOG(LOG_ERR, "pthread_create failed at " << status << __FILE__ << __LINE__);
				exit(status);
			}
		}
	}
	status = pthread_attr_destroy(&attr);

	if(status)
	{
		LOG(LOG_ERR, "pthread_attr_destroy failed at " << status << __FILE__ << __LINE__);
		exit(status);
	}
}

void* CThread::join()
{
	int status = pthread_join(m_thread, NULL);

	if(status)
	{
		LOG(LOG_ERR, "pthread_join failed at " << status << __FILE__ << __LINE__);
		exit(status);
	}
	return result;
}

void CThread::setCompleted()
{

}


