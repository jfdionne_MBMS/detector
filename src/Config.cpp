/*
 * Config.cpp
 *
 *  Created on: 2014-10-13
 *      Author: jfdionne
 */
#include "include/Config.h"
#include "include/LOG.h"
#include "include/MBMS.h"

#define LOG(lvl, args...) LOGMOD("CONFIG", lvl, args)

CConfig& CConfig::getInstance()
{
	static CConfig instance;
	return instance;
}

CConfig::CConfig()
{
	boost::property_tree::read_ini(CONFIG_FULLPATH_FILE, m_ptree);
}

string CConfig::getParameterValue(string strParamName)
{
	return m_ptree.get<string>(strParamName);
}

void CConfig::setParameterValue(string strParamName, string strValue)
{
	string test = getParameterValue(strParamName);
	if(strValue != test)
	{
		m_ptree.put(strParamName, strValue);
		boost::property_tree::write_ini(CONFIG_FULLPATH_FILE, m_ptree);

		listenerVector::iterator it;
		for(it = m_listenerVec.begin(); it != m_listenerVec.end(); ++it)
		{
			(*it)->configChangedNotify();
		}
	}
}
