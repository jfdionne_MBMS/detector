/*
 * ADC.cpp
 *
 *  Created on: 2014-10-11
 *      Author: jfdionne
 */
#include "include/ADC.h"
#include "include/LOG.h"
#include "include/IIOUtils.h"
#include <sstream>
#include <fstream>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <cerrno>
#include <time.h>
#include <stdlib.h>

#define LOG(lvl, args...) LOGMOD("ADC", lvl, args)

const string CADC::device_name = "max11040k";
const string CADC::trigger_name_base = "max11040k-dev";
const string CADC::trigger = "current_trigger";
const string CADC::configuration = "configuration";
const string CADC::sampling_instant = "sampling_instant";
const string CADC::data_rate = "data_rate";
const string CADC::buffer_length = "length";
const int32 CADC::buf_len = 200;

const uint16 CADC::DATA_RATE_4MS = 0x2600;
const uint16 CADC::DATA_RATE_2MS = 0x2000;
const uint16 CADC::DATA_RATE_1MS = 0x4000;
const uint16 CADC::DATA_RATE_0_5MS = 0x6000;
 const uint16 CADC::DATA_RATE_0_25MS = 0x8000;

CADC::CADC()
{
	int ret;
	ostringstream oss;

	m_dev_num = CIIOUtils::getInstance()->find_type_by_name(device_name, "iio:device");
	if(m_dev_num < 0)
	{
		LOG(LOG_ERR, "Failed to find " << device_name << endl);
		return;
	}
	LOG(LOG_INFO, "IIO device number is " << m_dev_num << endl);

	oss << CIIOUtils::getInstance()->iio_dir << "iio:device" << m_dev_num;
	m_dev_dir_name = oss.str();
	oss.str("");

	oss << m_dev_dir_name << "/trigger";
	m_trigger_dir_name = oss.str();
	oss.str("");

	oss << trigger_name_base << m_dev_num;
	m_trigger_name = oss.str();
	oss.str("");

	oss << m_dev_dir_name << "/buffer";
	m_buf_dir_name = oss.str();
	oss.str("");

	WriteSamplingInstant(0);

	WriteConfiguration(0x38);

	setDataRate((uint32)atoi(CConfig::getInstance().getParameterValue(CONFIG_SAMPLING_RATE).c_str()));
	setRecordTimeMs((uint32)(atoi(CConfig::getInstance().getParameterValue(CONFIG_RECORD_LENGTH_S).c_str()) * 1000));
	processBufferLength();

	m_bRunning = true;
	m_current_state = IDLE;
	sem_init(&m_startAcquisitionSem, 0, 0);
	sem_init(&m_timerSem, 0, 0);
	pthread_mutex_init(&m_stateMutex,0);
}

CADC::~CADC()
{
	pthread_mutex_destroy(&m_stateMutex);
}

void CADC::setBufferLength(int32 length)
{
	int ret = CIIOUtils::getInstance()->write_sysfs_int_and_verify(buffer_length, m_buf_dir_name, length);
	if (ret < 0)
	{
		LOG(LOG_ERR, "Unable to write buffer lenght" << endl);
	}
}

void CADC::WriteSamplingInstant(uint32 value)
{
	int ret = CIIOUtils::getInstance()->write_sysfs_int_and_verify(sampling_instant, m_dev_dir_name, value);
	if (ret < 0)
	{
		LOG(LOG_ERR, "Unable to write sampling instant" << endl);
	}
}

void CADC::WriteDataRate(uint16 value)
{
	int ret = CIIOUtils::getInstance()->write_sysfs_int_and_verify(data_rate, m_dev_dir_name, value);
	if (ret < 0)
	{
		LOG(LOG_ERR, "Unable to write data rate" << endl);
	}
	else
	{
		m_data_rate = value;
	}
}

void CADC::WriteConfiguration(uint8 value)
{
	int ret = CIIOUtils::getInstance()->write_sysfs_int_and_verify(configuration, m_dev_dir_name, value);
	if (ret < 0)
	{
		LOG(LOG_ERR, "Unable to write configuration" << endl);
	}
}

void CADC::configChangedNotify()
{
	setDataRate((uint32)atoi(CConfig::getInstance().getParameterValue(CONFIG_SAMPLING_RATE).c_str()));
	setRecordTimeMs((uint32)(atoi(CConfig::getInstance().getParameterValue(CONFIG_RECORD_LENGTH_S).c_str()) * 1000));
	processBufferLength();
	LOG(LOG_INFO, "Acquisition configuration changed!" << endl);
}

void CADC::setDataRate(uint32 samples_per_sec)
{
	switch(samples_per_sec)
	{
		case 250:
			m_data_rate = DATA_RATE_4MS;
		break;
		case 500:
			m_data_rate = DATA_RATE_2MS;
		break;
		case 1000:
			m_data_rate = DATA_RATE_1MS;
		break;
		case 2000:
			m_data_rate = DATA_RATE_0_5MS;
		break;
		case 4000:
			m_data_rate = DATA_RATE_0_25MS;
		break;
		default:
			LOG(LOG_ERR, "Impossible data rate, defaulting to 4ms" << endl);
			m_data_rate = DATA_RATE_4MS;
			break;
	}
	m_samples_per_sec = samples_per_sec;
	WriteDataRate(m_data_rate);
}

void CADC::setRecordTimeMs(uint32 record_duration_ms)
{
	m_record_duration_ms = record_duration_ms;
}

void CADC::processBufferLength()
{
	if(m_record_duration_ms)
	{
		int32 result = (int32)(m_samples_per_sec * (m_record_duration_ms / 1000));
		LOG(LOG_INFO, "Number of samples is " << result << endl);
		setBufferLength(result);
	}
	else
	{
		LOG(LOG_ERR, "Invalid duration..." << endl);
	}
}

void CADC::startRecording(bool realTimeMode)
{
	pthread_mutex_lock(&m_stateMutex);
	if(!realTimeMode && (m_current_state == IDLE))
	{
		m_current_state = ACQUIRE;
		sem_post(&m_startAcquisitionSem);
	}
	pthread_mutex_unlock(&m_stateMutex);
}

void CADC::stopRecording()
{
	pthread_mutex_lock(&m_stateMutex);
	if(m_current_state == ACQUIRE)
	{
		sem_post(&m_timerSem);
	}
	pthread_mutex_unlock(&m_stateMutex);
}

void CADC::stopAcquisitionThread()
{
	pthread_mutex_lock(&m_stateMutex);
	m_current_state = STOP_THREAD;
	switch(m_current_state)
	{
	case IDLE:
		sem_post(&m_startAcquisitionSem);
		break;
	case ACQUIRE:
		sem_post(&m_timerSem);
		break;
	}
	pthread_mutex_unlock(&m_stateMutex);
}

void* CADC::run()
{
	while(m_bRunning)
	{
		switch(m_current_state)
		{
			case IDLE:
				sem_wait(&m_startAcquisitionSem);
				break;
			case ACQUIRE:
				{
					struct timespec ts;
					int ret;

					if (clock_gettime(CLOCK_REALTIME, &ts) == -1)
					{
						LOG(LOG_ERR, "clock_gettime failed" << endl);
					}

					uint32 delay = m_record_duration_ms + 100;

					ts.tv_sec += delay / 1000;
					ts.tv_nsec += (delay % 1000) * 1000000;

					ret = CIIOUtils::getInstance()->write_sysfs_string_and_verify(trigger, m_trigger_dir_name, m_trigger_name);
					if(ret < 0)
					{
						LOG(LOG_ERR, "Failed to write current trigger file" << endl);
					}

					system("echo \"1\" > /sys/bus/iio/devices/iio:device0/buffer/enable");

					ret = sem_timedwait(&m_timerSem, &ts);

					system("echo \"0\" > /sys/bus/iio/devices/iio:device0/buffer/enable");

					CIIOUtils::getInstance()->write_sysfs_string(trigger, m_trigger_dir_name, "NULL");

					if(ret == -1 && errno == ETIMEDOUT)
					{
						LOG(LOG_INFO, "Acquisition successful!" << endl);
						m_pListener->ADCNotify(false);
					}
					else if(ret == 0)
					{
						LOG(LOG_INFO, "Acquisition stopped!" << endl);
						m_pListener->ADCNotify(true);
					}

					pthread_mutex_lock(&m_stateMutex);
					if(m_current_state == ACQUIRE)
						m_current_state = IDLE;
					pthread_mutex_unlock(&m_stateMutex);
				}
				break;
			case STOP_THREAD:
				m_bRunning = false;
				break;
		}
	}

	return 0;
}
