/*
 * LOG.h
 *
 *  Created on: 2014-10-10
 *      Author: jfdionne
 */

#ifndef LOG_H_
#define LOG_H_

#include <iostream>
#include <string>

using namespace std;

typedef enum
{
	LOG_EMERG = 0,
	LOG_ALERT,
	LOG_CRIT,
	LOG_ERR,
	LOG_WARN,
	LOG_NOTICE,
	LOG_INFO,
	LOG_DBG,
	LOG_MAX
}logLevels;

class CLOG
{
public:
	static CLOG& getInstance();
	string getTime();
	string strLevels[LOG_MAX];
private:
	CLOG();
	CLOG(CLOG const&);
	void operator=(CLOG const&);
};

#define LOGMOD(mod, lvl, args...) \
	{ 	if(lvl < LOG_DBG) { \
		cout << CLOG::getInstance().getTime() << "_" << mod << "_" << \
		CLOG::getInstance().strLevels[lvl] << "->" << args; } \
	}

#endif /* LOG_H_ */
