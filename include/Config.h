/*
 * Config.h
 *
 *  Created on: 2014-10-13
 *      Author: jfdionne
 */

#ifndef CONFIG_H_
#define CONFIG_H_
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <string>
#include <vector>

using namespace std;

class CConfigListener
{
public:
	virtual ~CConfigListener(){}
	virtual void configChangedNotify() = 0;
};

class CConfig
{
public:
	typedef vector<CConfigListener*> listenerVector;
	static CConfig& getInstance();
	void registerListener(CConfigListener* pListener) { m_listenerVec.push_back(pListener); }
	void setParameterValue(string strParamName, string strValue);
	string getParameterValue(string strParamName);
private:
	CConfig();
	CConfig(CConfig const&);
	void operator=(CConfig const&);
	boost::property_tree::ptree m_ptree;
	listenerVector m_listenerVec;
};

#endif /* CONFIG_H_ */
