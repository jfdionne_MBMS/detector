/*
 * FTPClient.h
 *
 *  Created on: 2014-10-12
 *      Author: jfdionne
 */

#ifndef FTPCLIENT_H_
#define FTPCLIENT_H_

#include <curl/curl.h>
#include <string>

using namespace std;

class CFTPClient
{
public:
	static CFTPClient& getInstance();
	void upload(string filename);
private:
	CFTPClient();
	CFTPClient(CFTPClient const&);
	void operator=(CFTPClient const&);
	static size_t read_callback(void* ptr, size_t size, size_t nmemb, void* stream);
	static const string RENAME_FROM_FTP_CMD;
	static const string RENAME_TO_FTP_CMD;
	static const string FILE_TO_TRANFER_PATH;
	static const string REMOTE_URL;
	CURL* m_curl;
	CURLcode m_res;
};

#endif /* FTPCLIENT_H_ */
