/*
 * FileManager.h
 *
 *  Created on: Oct 24, 2015
 *      Author: jfdionne
 */

#ifndef FILEMANAGER_H_
#define FILEMANAGER_H_

#include <set>
#include <fstream>
#include "MBMS.h"
#include "Config.h"

using namespace std;

struct classcomp {
  bool operator() (const string& lhs, const string& rhs) const
  {return lhs<rhs;}
};

typedef set<string, classcomp> stringSet;

class CFileManager : public CConfigListener
{
public:
	static CFileManager& getInstance();
	bool init();
	void createSEGYBaseFile();
	void appendSEGYTrace(uint32* data, uint32 length);
	void closeSEGYFile();

	void configChangedNotify();

	static const string filesBaseDir;
	static const string filesBaseName;
	static const string filesExtension;
	stringSet segyFileSet;
	ofstream ofs;
	string m_currentFilename;
private:
	uint32 m_samplingRate;
	uint16 m_sampleIntervalUs;
	uint32 m_recordLength;
	uint16 m_sampleCount;

	int32 m_currentFileTraceIndex;
	CFileManager();
	CFileManager(CFileManager const&);
	void operator=(CFileManager const&);
};

#endif /* FILEMANAGER_H_ */
