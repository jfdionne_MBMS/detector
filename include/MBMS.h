/*
 * MBMS.h
 *
 *  Created on: 2014-10-19
 *      Author: jfdionne
 */

#ifndef MBMS_H_
#define MBMS_H_
#include <string>

using namespace std;

typedef unsigned char uint8;
typedef char int8;
typedef unsigned short uint16;
typedef short int16;
typedef unsigned int uint32;
typedef int int32;
typedef unsigned long long uint64;
typedef long long int64;

//System defines
const uint16 NUMBER_OF_CHANNELS = 3;
const string SERVER_IPADDRESS = "192.168.0.66";
const int32 SERVER_PORT = 4510;

//FTP config
const string USERNAME = "jfdionne";
const string PASS = "slayer";

//Configuration defines
const string CONFIG_FULLPATH_FILE = "/home/config.ini";
const string CONFIG_SAMPLING_RATE = "sampling";
const string CONFIG_RECORD_LENGTH_S = "reclen";

//TCP command messages
const string COMMAND_CONF = "conf";
const string COMMAND_ACQ_DEF = "acqdef"; 	//Acquisition based on a timer
const string COMMAND_ACQ_RT	= "acqrt";		//Real-time acquistion
const string COMMAND_ACQ_START = "start";
const string COMMAND_ACQ_STOP = "stop";

#endif /* MBMS_H_ */
