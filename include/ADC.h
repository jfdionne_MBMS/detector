/*
 * ADC.h
 *
 *  Created on: 2014-10-11
 *      Author: jfdionne
 */

#ifndef ADC_H_
#define ADC_H_

#include "Thread.h"
#include "MBMS.h"
#include "Config.h"
#include <string>
#include <semaphore.h>

using namespace std;

class CADCListener
{
public:
	virtual ~CADCListener(){}
	virtual void ADCNotify(bool bCompletedOrStopped) = 0;
};

class CADC : public CThread, public CConfigListener
{
public:
	typedef enum
	{
		IDLE = 0,
		ACQUIRE,
		STOP_THREAD
	}AcquisitionState;

	CADC();
	~CADC();
	AcquisitionState getCurrentState(){ return m_current_state; }
	void setDataRate(uint32 samples_per_sec);
	void setRecordTimeMs(uint32 record_duration_ms);
	void startRecording(bool realTimeMode);
	void stopRecording();
	void stopAcquisitionThread();
	void registerListener(CADCListener* pListener)
		{ m_pListener = pListener; }

	void* run();
	void configChangedNotify();

	int m_dev_num;
	uint32 m_samples_per_sec;
	uint32 m_record_duration_ms;
private:
	static const string device_name;
	static const string trigger_name_base;
	static const string trigger;
	static const string configuration;
	static const string sampling_instant;
	static const string data_rate;
	static const string buffer_length;
	static const int32 buf_len;

	static const uint16 DATA_RATE_4MS;
	static const uint16 DATA_RATE_2MS;
	static const uint16 DATA_RATE_1MS;
	static const uint16 DATA_RATE_0_5MS;
	static const uint16 DATA_RATE_0_25MS;

	bool m_bRunning;
	string m_dev_dir_name;
	string m_trigger_dir_name;
	string m_trigger_name;
	string m_buf_dir_name;
	uint16 m_data_rate;
	AcquisitionState m_current_state;
	sem_t m_startAcquisitionSem;
	sem_t m_timerSem;
	pthread_mutex_t m_stateMutex;

	CADCListener* m_pListener;

	void WriteSamplingInstant(uint32 value);
	void WriteDataRate(uint16 value);
	void WriteConfiguration(uint8 value);
	void setBufferLength(int32 length);
	void processBufferLength();
};

#endif /* ADC_H_ */
