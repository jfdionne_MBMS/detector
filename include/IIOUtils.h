/*
 * IIOUtils.h
 *
 *  Created on: 2014-11-21
 *      Author: jfdionne
 */
#include <string>
#include "MBMS.h"

#ifndef IIOUTILS_H_
#define IIOUTILS_H_

using namespace std;

struct iio_event_data {
	int32 id;
	int64 timestamp;
};

class CIIOUtils
{
public:
	static const int32 IIO_EVENT_CODE_RING_50_FULL;
	static const int32 IIO_EVENT_CODE_RING_75_FULL;
	static const int32 IIO_EVENT_CODE_RING_100_FULL;
	static CIIOUtils* getInstance();
	static const string iio_dir;
	int32 find_type_by_name(string name, string type);
	int32 write_sysfs_int(string filename, string basedir, int32 val);
	int32 write_sysfs_int_and_verify(string filename, string basedir, int32 val);
	int32 write_sysfs_string_and_verify(string filename, string basedir, string val);
	int32 write_sysfs_string(string filename, string basedir, string val);
	int32 read_sysfs_posint(string filename, string basedir);
	int32 read_sysfs_float(string filename, string basedir, float *val);
private:
	CIIOUtils(){}
	~CIIOUtils();
	static const int32 IIO_MAX_NAME_LENGTH;
	static CIIOUtils* m_instance;
	int32 write_sysfs_int(string filename, string basedir, int32 val, int32 verify);
	int32 write_sysfs_string(string filename, string basedir, string val, int32 verify);
};


#endif /* IIOUTILS_H_ */
