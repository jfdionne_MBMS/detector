/*
 * SEGY.h
 *
 *  Created on: Oct 19, 2015
 *      Author: jfdionne
 */

#ifndef SEGY_H_
#define SEGY_H_

const long TEXTUAL_FILE_HEADER_LENGTH = 3200;

int8 TextualFileHeader[TEXTUAL_FILE_HEADER_LENGTH];

#pragma pack(push, 1)
typedef struct
{
	uint32 jobIdentificationNumber;
	uint32 lineNumber;
	uint32 reelNumber;
	uint16 dataTracesPerRecord;	//To fill
	uint16 auxTracesPerRecord;
	uint16 sampleIntervalForReelUs;	//To fill
	uint16 sampleIntervalForFieldUs;	//To fill
	uint16 numberOfSamplesPerTraceReel;
	uint16 numberOfSamplesPerTraceField;
	uint16 dataSampleFormatCode;	//To fill
	uint16 CDPFold;
	uint16 traceSortingCode;
	uint16 verticalSumCode;
	uint16 sweepFrequencyAtStart;
	uint16 sweepFrequencyAtEnd;
	uint16 sweepLengthMs;
	uint16 sweepTypeCode;
	uint16 traceNumberOfSweepChannel;
	uint16 sweepTaperAtStartMs;
	uint16 sweepTaperAtEndMs;
	uint16 taperType;
	uint16 correlatedDataTraces;
	uint16 binaryGainRecovered;
	uint16 amplitudeRecoveryMethod;
	uint16 measurementSystem;
	uint16 impulseSignal;
	uint16 vibratoryPolarityCode; //59-60
	uint8 unused1[240];
	uint16 segyRev;
	uint16 fixedLengthTraceFlag;
	uint16 numberOfExtendedTextualHeaders;
	uint8 unused2[94];
}BinaryHeader;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct
{
	uint32 traceSequenceNumberWithinLine;
	uint32 traceSequenceNumberWithinReel;
	uint32 originalFieldRecordNumber;
	uint32 traceNumberWithinFieldRecord;
	uint32 energySourcePointNumber;
	uint32 ensembleNumber;
	uint32 traceNumberWithinEnsemble; //28
	uint16 traceIdentificationCode;
	uint16 verticallySummedTraces;
	uint16 horizontallyStackedTraces;
	uint16 dataUse;	//36
	uint32 distanceFromSourcePoint;
	uint32 receiverGroupElevation;
	uint32 surfaceElevationAtSource;
	uint32 sourceDepthBelowSurface;
	uint32 datumElevationAtReceiverGroup;
	uint32 datumElevationAtSource;
	uint32 waterDepthAtSource;
	uint32 waterDepthAtGroup;	//68
	uint16 scalarToAllElevations;
	uint16 scalarToAllCoordinates;	//72
	uint32 sourceXCoordinate;
	uint32 sourceYCoordinate;
	uint32 groupXCoordinate;
	uint32 groupYCoordinate;	//88
	uint16 coordinateUnits;
	uint16 weatheringVelocity;
	uint16 subweatheringVelocity;
	uint16 upholeTimeAtSource;
	uint16 upholeTimeAtGroup;	//98
	uint16 sourceStaticCorrection;
	uint16 groupStaticCorrection;
	uint16 totalStaticApplied;
	uint16 lagTimeA;
	uint16 lagTimeB;	//108
	uint16 dealyRecordingTime;
	uint16 muteTimeStart;
	uint16 muteTimeEnd;		//114
	uint16 numberOfSamplesInThisTrace;	//To fill
	uint16 sampleIntervalInMsForThisTrace;	//To fill
	uint16 gainTypeOfFieldInstruments;
	uint16 instrumentGain;
	uint16 instrumentGainConstant;
	uint16 correlated;	//126
	uint16 sweepFrequencyAtStart;
	uint16 sweepFrequencyAtEnd;
	uint16 sweepLengthinMs;
	uint16 sweepType;
	uint16 sweepTaperAtStartMs;
	uint16 sweepTaperAtEndMs;
	uint16 taperType;	//140
	uint16 aliasFilterFrequency;
	uint16 aliasFilterSlope;
	uint16 notchFilterFrequency;
	uint16 notchFilterSlope;
	uint16 lowCutFrequency;
	uint16 highCutFrequency;
	uint16 lowcutSlope;
	uint16 highCutSlope;
	uint16 yearDataRecorded;	//158
	uint16 dayOfYear;
	uint16 hourOfDay;
	uint16 minuteOfHour;
	uint16 secondOfMinute;
	uint16 timeBasisCode;
	uint16 traceWeightingFactor;	//170
	uint16 geophoneGroupNumberOfRollSwitch;
	uint16 geophoneGroupNumberOfTraceOne;
	uint16 geophoneGroupNumberOfLastTrace;
	uint16 gapSize;
	uint16 overTravelTaper; //180
	uint32 CDPX;
	uint32 CDPY;
	uint32 inlineNumber;
	uint32 crosslineNumber;
	uint32 shotpointNumber;	//200
	uint16 shotpointScalar;
	uint16 traceValueMeasurementUnit;	//204
	uint8 transductionConstant[6];
	uint16 transductionUnits;
	uint16 deviceTraceID;
	uint16 traceHeaderTimeScalar;
	uint16 sourceTypeOrientation;	//218
	uint8 sourceEnergyDirection[6];
	uint8 sourceMeasurement[6];		//230
	uint16 sourceMeasurementUnit;
	uint8 unused[8];	//240
}TraceHeader;
#pragma pack(pop)

#endif /* SEGY_H_ */
