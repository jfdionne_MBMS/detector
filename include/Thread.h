/*
 * Thread.h
 *
 *  Created on: 2014-10-11
 *      Author: jfdionne
 */

#ifndef THREAD_H_
#define THREAD_H_

#include <pthread.h>
#include <stdlib.h>
#include <iostream>
#include <memory>

using namespace std;

class CRunnable
{
public:
	virtual void* run() = 0;
	virtual ~CRunnable(){}
};

class CThread
{
public:
	CThread(auto_ptr<CRunnable> run, bool isDetached = false);
	CThread(bool isDetached = false) : runnable(NULL), detached(isDetached), result(NULL) {}
	virtual ~CThread(){}
	void start();
	void* join();
private:
	pthread_t m_thread;
	pthread_attr_t attr;
	auto_ptr<CRunnable> runnable;
	bool detached;
	CThread(const CThread&);
	const CThread& operator=(const CThread&);
	void setCompleted();
	void* result;
	virtual void* run() = 0;// {return 0;}
	static void* startThreadRunnable(void* pVoid);
	static void* startThread(void* pVoid);
};

#endif /* THREAD_H_ */
