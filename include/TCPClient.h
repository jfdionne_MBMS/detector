/*
 * TCPClient.h
 *
 *  Created on: Sep 28, 2015
 *      Author: jfdionne
 */

#ifndef TCPCLIENT_H_
#define TCPCLIENT_H_

#include "Thread.h"
#include "MBMS.h"
#include <string>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <queue>

using namespace std;

typedef struct
{
	int8 buffer[256];
	int32 length;
}TCPBuffer, *pTCPBuffer;

class CTCPListener
{
public:
	virtual ~CTCPListener(){}
	virtual void TCPNotify() = 0;
};

class CTCPClient : public CThread
{
private:
	queue<pTCPBuffer> m_pTCPBufferQueue;
	typedef enum
	{
		WAIT_FOR_CONNECT = 0,
		CONNECTED,
		STOP_CLIENT,
	}CommState;
public:
	CTCPClient(string ipaddress, int32 port);
	~CTCPClient();
	void* run();
	void stopClient() { m_state = STOP_CLIENT; }
	void registerListener(CTCPListener* pListener)
		{ m_pListener = pListener; }
	pTCPBuffer getLastMessage();
private:
	static const int32 RECEIVE_TIMEOUT;
	struct sockaddr_in m_address;
	int32 m_socket;
	CommState m_state;
	CTCPListener* m_pListener;
	bool m_clientRunning;
};


#endif /* TCPCLIENT_H_ */
