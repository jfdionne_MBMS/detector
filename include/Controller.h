/*
 * Controller.h
 *
 *  Created on: Sep 29, 2015
 *      Author: jfdionne
 */

#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#include "TCPClient.h"
#include "ADC.h"
#include <semaphore.h>

class CController : public CThread, public CTCPListener, public CADCListener
{
public:
	CController();
	~CController();
	void ADCNotify(bool bCompletedOrStopped);
	void TCPNotify();
	void* run();
	void stopController();
	auto_ptr<CTCPClient> m_pClientThread;
	auto_ptr<CADC> m_pADCThread;
private:
	typedef enum
	{
		INIT = 0,
		ACQUIRE_DEF,
		ACQUIRE_RT,
	}ControllerState;

	ControllerState currentState;
	TCPBuffer* m_pTCPBuffer;
	FILE* m_buffer_handler;
	string m_buf_name;
	sem_t m_SemReact;

	uint8 m_datum[12];
	uint32* channel0Vals;
	uint32* channel1Vals;
	uint32* channel2Vals;

	bool m_bRunning;
	bool m_bAcqCompletedOrStopped;
};

#endif /* CONTROLLER_H_ */
